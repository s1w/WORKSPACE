//
//  MyContainerNavigationViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/14/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class MyContainerNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.isHidden = true
    }

}
