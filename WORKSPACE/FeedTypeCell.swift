//
//  FeedTypeCell.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/5/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

// Ячейка содержащая в себе UICollectionView в котором будет сам фид.
// В FeedViewController есть два вида фида, один со всеми проектами, второй - со своими.
// Для этого и используется две ячейки типа FeedTypeCell с ячейками FeedType внутри.

class FeedTypeCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    private let feedCellID = "FeedCell"
    weak var delegateController: FeedViewController?
    
    var tickets = [Ticket]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tickets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: feedCellID, for: indexPath) as! FeedCell
        cell.ticket = self.tickets[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProjectInfoViewController") as! ProjectInfoViewController
        vc.ticket = self.tickets[indexPath.item]
        self.delegateController?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
