//
//  FeedViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/5/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit
import SwiftyJSON

enum ProjectIndicatorPosition {
    case left
    case right
}

class FeedViewController: UIViewController {

    fileprivate let feedTypeCell = "FeedTypeCell"
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var positionIndicator: CUIView!
    @IBOutlet weak var allProjectsButton: UIButton!
    @IBOutlet weak var myProjectsButton: UIButton!
    @IBOutlet weak var addTicketButton: CUIButton!
    
    fileprivate var allTickets = [Ticket]()
    fileprivate var myTickets = [Ticket]()
    
    weak var menu: MyMenuViewController?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getFeeds()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    private func setupView() {
        self.getMyProfileInfo()
        
//        WSApi.shared.getTicket(id: "1033") { (json, error) in
//            if let error = error {
//                print(error)
//                return
//            }
//            print(json)
//        }
        
//        WSApi.shared.createTicketComment(ticketId: "1033", text: "I want this project!!!!!!", money: 900, numberOfDays: 5) { (isCommented, error) in
//            if let error = error {
//                print(error)
//                return
//            }
//            print(isCommented)
//        }
    }
    
    @IBAction func openAllProjects(_ sender: UIButton) {
        let indexPath = IndexPath(item: 0, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.animatePositionIndicator(to: .left)
    }
    @IBAction func openMyProjects(_ sender: UIButton) {
        let indexPath = IndexPath(item: 1, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.animatePositionIndicator(to: .right)
    }
    @IBAction func addProject(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddTicketViewController") as! AddTicketViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func openMenu(_ sender: Any) {
        self.menu?.menuTapped()
    }
    
    func animatePositionIndicator(to: ProjectIndicatorPosition) {
        let originX: CGFloat!
        let width: CGFloat!
        switch to {
        case .left:
            originX = self.allProjectsButton.frame.origin.x
            width = self.allProjectsButton.frame.width
        case .right:
            originX = self.myProjectsButton.frame.origin.x
            width = self.myProjectsButton.frame.width
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
            self.positionIndicator.frame = CGRect(x: originX, y: self.positionIndicator.frame.origin.y, width: width, height: self.positionIndicator.frame.height)
        }, completion: nil)
    }
    
    func getMyProfileInfo() {
        WSApi.shared.getMyProfile { (json, error) in
            if let error = error {
                print(error)
                return
            }
            if json["profile"]["userType"].stringValue == UserType.executor.rawValue {
                self.addTicketButton.isHidden = true
            } else if json["profile"]["userType"].stringValue == UserType.client.rawValue {
                self.addTicketButton.isHidden = false
            }
        }
    }
    
    func getFeeds() {
        self.allTickets = [Ticket]()
        WSApi.shared.getAllTickets(step: 0) { (json, error) in
            if let error = error {
                print(error)
                return
            }
            for data in json["feed"] {
                let ticket = Ticket(data: data.1)
                self.allTickets.append(ticket)
            }
            self.collectionView.reloadItems(at: [IndexPath(item: 0, section: 0)])
        }
        
        self.myTickets = [Ticket]()
        WSApi.shared.getMyTickets { (json, error) in
            if let error = error {
                print(error)
                return
            }
            for data in json["myFeed"] {
                let ticket = Ticket(data: data.1)
                self.myTickets.append(ticket)
            }
            self.collectionView.reloadItems(at: [IndexPath(item: 1, section: 0)])
        }
    }
    
}


// MARK: - Collection view delegate

extension FeedViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: feedTypeCell, for: indexPath) as! FeedTypeCell
        cell.delegateController = self
        cell.tickets = indexPath.item == 0 ? allTickets : myTickets
        return cell
    }
    
}



















































