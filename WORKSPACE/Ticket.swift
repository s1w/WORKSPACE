//
//  Ticket.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/14/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import Foundation
import SwiftyJSON

class Ticket {
    var id: Int
    var title: String
    var text: String
    var category: String
    var underCategory: String
    var price: Float
    var date: TimeInterval
    
    init() {
        self.id = 0
        self.title = ""
        self.text = ""
        self.category = ""
        self.underCategory = ""
        self.price = 0
        self.date = 0
    }
    
    init(data: JSON) {
        self.id = data["id"].intValue
        self.title = data["title"].stringValue
        self.text = data["text"].stringValue
        self.category = data["category"].stringValue
        self.underCategory = data["underCategory"].stringValue
        self.price = data["price"].floatValue
        self.date = data["date"].doubleValue
    }
}
