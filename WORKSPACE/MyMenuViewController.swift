//
//  MyMenuViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/14/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class MyMenuViewController: UIViewController {
    
    var feedViewController: FeedViewController!
    var containerNavigationController: MyContainerNavigationViewController!
    var screenSwipeGesture: UIPanGestureRecognizer!
    var isMenuOpened = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupContainer()
    }
    
    private func setupContainer() {
        self.screenSwipeGesture = UIPanGestureRecognizer(target: self, action: #selector(screenSwiped))
        
        self.containerNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyContainerNavigationViewController") as! MyContainerNavigationViewController
        
        self.addChildViewController(self.containerNavigationController)
        self.view.addSubview(self.containerNavigationController.view)
        self.containerNavigationController.didMove(toParentViewController: self)
        self.containerNavigationController.view.frame = self.view.bounds
        
        self.containerNavigationController.view.addGestureRecognizer(screenSwipeGesture)
        
        feedViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
        feedViewController.menu = self
        self.containerNavigationController.viewControllers = [feedViewController]
        
        self.containerNavigationController.view.layer.shadowColor = UIColor.black.cgColor
        self.containerNavigationController.view.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.containerNavigationController.view.layer.shadowOpacity = 0.5
        self.containerNavigationController.view.layer.shadowRadius = 3
    }
    
    @objc private func screenSwiped(recognizer: UIPanGestureRecognizer) {
        self.view.endEditing(true)
        let t = recognizer.translation(in: self.view)
        switch recognizer.state {
        case .changed:
            if isMenuOpened {
                containerNavigationController.view.transform = CGAffineTransform(translationX: self.view.frame.width * 0.7 + t.x, y: 0)
            } else {
                if t.x > 0 {
                    containerNavigationController.view.transform = CGAffineTransform(translationX: t.x, y: 0)
                }
            }
        case .ended:
            if !isMenuOpened {
                t.x > view.frame.width * 0.35 ? openMenu() : closeMenu()
            } else {
                -t.x > view.frame.width * 0.25 ? closeMenu() : openMenu()
            }
        default: return
        }
    }
    
    private func closeMenu() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
            self.containerNavigationController.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }, completion: nil)
        containerNavigationController.interactivePopGestureRecognizer?.isEnabled = true
        isMenuOpened = false
    }
    
    private func openMenu() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
            self.containerNavigationController.view.transform = CGAffineTransform(translationX: self.view.frame.width * 0.7, y: 0)
        }, completion: nil)
        containerNavigationController.interactivePopGestureRecognizer?.isEnabled = false
        isMenuOpened = true
    }
    
    public func menuTapped() {
        isMenuOpened ? closeMenu() : openMenu()
    }
    
    @IBAction func openFeed(_ sender: Any) {
        self.containerNavigationController.viewControllers = [feedViewController]
        self.closeMenu()
    }
    @IBAction func openProf(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrangeViewController") as! OrangeViewController
        vc.menu = self
        self.containerNavigationController.viewControllers = [vc]
        self.closeMenu()
    }
    @IBAction func logOut(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyMenuNavigationController") as! MyMenuNavigationController
        UserDefaults.standard.logOut()
        self.present(vc, animated: true, completion: nil)
    }
    
}






































