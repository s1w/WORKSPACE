//
//  FeedCell.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/5/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class FeedCell: UICollectionViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var projectInfo: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var ticket: Ticket? {
        didSet {
            
            if let ticket = ticket {
                self.titleLable.text = ticket.title
                self.projectInfo.text = "\(ticket.category) • \(ticket.underCategory)"
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM YYYY"
                self.dateLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: ticket.date/1000))
            }
        }
    }

}
