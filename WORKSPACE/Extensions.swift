//
//  Extensions.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/4/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}

extension UserDefaults {
    
    enum UserDefaultsKeys: String {
        case isLoggedIn
    }
    
    func setIsLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        synchronize()
    }
    
    func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func logOut() {
        print("handle log out")
        UserDefaults.standard.setIsLoggedIn(value: false)
        UserDefaults.standard.synchronize()
    }
    
    func logIn() {
        print("handle log in")
        UserDefaults.standard.setIsLoggedIn(value: true)
        UserDefaults.standard.synchronize()
    }
}

extension UIViewController {
    func endEdit() {
        self.view.endEditing(true)
    }
    
    func setEndEditOnTap() {
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEdit)))
    }
    
}

extension Notification.Name {
    static let feedViewControllerSelected = Notification.Name("feedViewControllerSelected")
    static let orangeVCSelected = Notification.Name("orangeVCSelected")
    static let logOutSelected = Notification.Name("logOutSelected")
}
