//
//  TickerToCreate.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/14/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import Foundation

class TicketToCreate {
    
    var title: String
    var text: String
    var category: String
    var underCategory: String
    var price: Float
    
    init() {
        self.title = ""
        self.text = ""
        self.category = ""
        self.underCategory = ""
        self.price = 0
    }
    
}
