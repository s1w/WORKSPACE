//
//  TicketPost.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/15/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import Foundation
import SwiftyJSON

class TicketPost {
    var money: Float
    var mobilePhone: String
    var name: String
    var numberOfDays: Int
    var text: String
    
    init() {
        self.money = 0
        self.mobilePhone = ""
        self.name = ""
        self.numberOfDays = 0
        self.text = ""
    }
    
    init(data: JSON) {
        self.money = data["money"].floatValue
        self.mobilePhone = data["mobilePhone"].stringValue
        self.name = data["name"].stringValue
        self.numberOfDays = data["numberOfDays"].intValue
        self.text = data["text"].stringValue
    }
}

