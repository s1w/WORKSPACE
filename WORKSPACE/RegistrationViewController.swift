//
//  RegistrationViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/4/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit
import SwiftyJSON

enum RegistrationStage {
    case mobilePhone
    case detailInformation
}

enum UserType: String {
    case client
    case executor
}

class RegistrationViewController: UIViewController {

    @IBOutlet var userInfoForm                  : UIView!
    @IBOutlet weak var logoView                 : UIImageView!
    @IBOutlet weak var phoneTextField           : UITextField!
    @IBOutlet weak var cityCodeSelectionButton  : UIButton!
    @IBOutlet weak var registrationButton       : UIButton!
    @IBOutlet weak var smsTextField             : SMSCodeTextField!
    @IBOutlet weak var newUserLabel             : UILabel!
    @IBOutlet weak var phoneSeparator           : UIView!
    @IBOutlet weak var cityCodeSeparator        : UIView!
    
    @IBOutlet weak var nameTextField    : UITextField!
    @IBOutlet weak var userType         : UISegmentedControl!
    @IBOutlet weak var passwordField    : UITextField!
    @IBOutlet weak var rePasswordField  : UITextField!
    
    var registerUser: RegistrationUser?
    var stage: RegistrationStage = .mobilePhone
    var cityCode: String = "380"
    var actionCode: String!
    var activationRegex: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandler), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandler), name: .UIKeyboardWillHide, object: nil)
        self.setEndEditOnTap()
    }

    @IBAction func loginTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registrationTapped(_ sender: UIButton) {
        switch stage {
        case .mobilePhone:
            self.sendSmsCode()
        case .detailInformation:
            self.showUserInfoForm()
        }
    }
    
    @IBAction func continueRegistrationTapped(_ sender: UIButton) {
        self.continueRegistration()
    }
    
    @IBAction func cityCodeSelectionTapped(_ sender: UIButton) {
        SPicker().show(doneTitle: "Готово", cancelTitle: "Отмена", rows: ["+380"]) { (selectedCode) in
            if let code = selectedCode {
                self.cityCode = code.replacingCharacters(in: code.range(of: "+")!, with: "")
            }
        }
    }
    
    func showUserInfoForm() {
        
        if self.smsTextField.code.isEmpty {
            self.showAlert(withMessage: "Введіть код з смс")
            return
        }
        
        self.userInfoForm.frame.origin = CGPoint(x: 0, y: self.view.frame.height)
        
        if !self.view.subviews.contains(userInfoForm) {
            self.view.addSubview(userInfoForm)
        }
        
        let userInfoFormHeight = self.view.frame.height - 65
        self.userInfoForm.frame = CGRect(x: 0, y: self.view.frame.height,
                                         width: self.view.frame.width, height: userInfoFormHeight)
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
            self.logoView.frame.origin = CGPoint(x: self.logoView.frame.origin.x, y: 15)
            self.userInfoForm.frame.origin = CGPoint(x: 0, y: self.view.frame.height - userInfoFormHeight)
            self.logoView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }, completion: nil)
    }
    
    func hideUserInfoForm() {
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
            self.logoView.frame.origin = CGPoint(x: self.logoView.frame.origin.x, y: 78)
            self.logoView.transform = CGAffineTransform(scaleX: 1, y: 1)
            
            self.userInfoForm.frame.origin = CGPoint(x: 0, y: self.view.frame.height)
        }, completion: nil)
    }
    
    private func sendSmsCode() {
        guard let phone = self.phoneTextField.text else { return }
        if phone.isEmpty {
            self.showAlert(withMessage: "Введіть свій номер телефону на який буде висланий код")
            return
        }
        
        self.registerUser = RegistrationUser()
        self.registerUser!.mobilePhone = cityCode+phone
        self.registrationButton.isEnabled = false
        
        WSApi.shared.eazyPayRegistration(mobilePhone: self.registerUser!.mobilePhone) { (response, error) in
            
            self.registrationButton.isEnabled = true
            
            if let error = error {
                guard let errorMessage = error.userInfo["error_name"] else { return }
                self.showAlert(withMessage: errorMessage as! String)
                return
            }
            
            if response.null == nil {
                self.stage = .detailInformation
                self.actionCode = response["ActionCode"].stringValue
                self.activationRegex = response["ActivationRegex"].stringValue
                
                let codeLength = Int(self.activationRegex) ?? 0
                if codeLength == 0 { return }
                
                self.smsTextField.codeLength = codeLength
                self.smsTextField.alpha = 1
                self.smsTextField.openKeyboard()
            }
        }
    }
    
    private func continueRegistration() {
        if registerUser == nil { return }
        
        guard let name = nameTextField.text else { return }
        guard let password = passwordField.text else { return }
        guard let rePassword = rePasswordField.text else { return }
        
        var errorMessage = ""
        
        if name.isEmpty {
            errorMessage = "Введіть своє ім'я"
        } else if password.isEmpty {
            errorMessage = "Введіть пароль"
        } else if rePassword.isEmpty {
            errorMessage = "Підтвердіть пароль"
        }
        
        self.showAlert(withMessage: errorMessage)
        
        self.registerUser?.name = name
        self.registerUser?.userType = userType.selectedSegmentIndex == 0 ? UserType.client.rawValue : UserType.executor.rawValue
        self.registerUser?.password = password
        self.registerUser?.rePassword = rePassword
        self.registerUser?.sms = self.smsTextField.code
        self.registerUser?.activationId = self.actionCode
        self.registerUser?.mobilePhone = cityCode+self.phoneTextField.text!
        WSApi.shared.registration(user: self.registerUser!) { (isRegistered, error) in
            if isRegistered {
                let vc = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyMenuNavigationController") as! MyMenuNavigationController
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func showAlert(withMessage: String) {
        if !withMessage.isEmpty {
            let alert = UIAlertController(title: "Ой, щось не так 🤔", message: withMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func keyboardHandler(notification: Notification) {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        guard let keyboardHeight = keyboardSize?.height else { return }
        if notification.name == .UIKeyboardWillShow {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.newUserLabel.alpha = 0
                self.registrationButton.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight*0.8)
                self.cityCodeSelectionButton.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight*0.5)
                self.phoneTextField.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight*0.5)
                self.smsTextField.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight*0.5)
                self.phoneSeparator.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight*0.5)
                self.cityCodeSeparator.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight*0.5)
            }, completion: nil)
        } else if notification.name == .UIKeyboardWillHide {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.newUserLabel.alpha = 1
                self.registrationButton.transform = CGAffineTransform.identity
                self.cityCodeSelectionButton.transform = CGAffineTransform.identity
                self.phoneTextField.transform = CGAffineTransform.identity
                self.smsTextField.transform = CGAffineTransform.identity
                self.phoneSeparator.transform = CGAffineTransform.identity
                self.cityCodeSeparator.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
}















































