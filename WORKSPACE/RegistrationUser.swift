//
//  RegistrationUser.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/6/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import Foundation

class RegistrationUser {
    var name: String
    var userType: String
    var password: String
    var rePassword: String
    var activationId: String
    var mobilePhone: String
    var sms: String
    
    init() {
        self.name = ""
        self.userType = ""
        self.password = ""
        self.rePassword = ""
        self.activationId = ""
        self.mobilePhone = ""
        self.sms = ""
    }
}


