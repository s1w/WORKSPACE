//
//  AddCommentViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/15/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class AddCommentViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    var ticket: Ticket!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEndEditOnTap()
    }
    @IBAction func sendComment(_ sender: Any) {
        guard let text = self.textField.text else { return }
        if text.isEmpty { return }
        self.sendButton.isEnabled = false
        WSApi.shared.createTicketComment(ticketId: "\(self.ticket.id)", text: text, money: 666, numberOfDays: 777) { (isCommented, error) in
            self.sendButton.isEnabled = true
            if let error = error {
                print(error)
                return
            }
            if isCommented {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    @IBAction func handleBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
