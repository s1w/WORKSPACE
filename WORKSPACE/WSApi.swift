//
//  WSApi.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/6/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import KeychainSwift

class WSApi {
    
    static let shared = WSApi()
    
    typealias WSResponse = JSON
    typealias WSError = NSError
    typealias WSResponseHandler = (WSResponse, WSError?) -> Void
    typealias WSBoolResponseHandler = (Bool, WSError?) -> Void
    
    private let host        = "http://45.55.219.163/" //http://45.55.219.163/     http://109.86.86.116:8080/
    private let iOSLogin    = "HZoe9RouNzf2P1Pk8zVG4tUenThL8FcJFEX5nMmQXtzcyGXlLBj7Jk0167nsVPTKVF8M2n"
    private let iOSPass     = "pqQutAh5tqac6p0YohIrbqe3f3J75sOPoijYCjiF91NlQnGr9seu2AHMn4Yy8pXG2y6n0b"
    
    public func login(phone: String, password: String, completion: @escaping WSBoolResponseHandler) {
        let credentialData = "\(iOSLogin):\(iOSPass)".data(using: .utf8)!
        let base64Credentials = credentialData.base64EncodedString()
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        let path = "oauth/token?grant_type=password&username=\(phone)&password=\(password)"
        fireRequset(url: host+path,
                    method: .post,
                    parameters: nil,
                    headers: headers)
        { (json) in
            if let token = json["access_token"].string {
                KeychainSwift().set(token, forKey: "access_token")
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "NO TOKEN RECIEVED", code: 0, userInfo: nil))
            }
        }
    }
    
    public func registration(user: RegistrationUser, completion: @escaping WSBoolResponseHandler) {
        let parameters: Parameters = [
            "password": user.password,
            "repeatPassword": user.rePassword,
            "mobilePhone": user.mobilePhone,
            "name": user.name,
            "userType": user.userType,
            "activationId": user.activationId,
            "sms": user.sms
        ]
        print(parameters)
        let headers = ["Content-Type": "application/json"]
        fireRequset(url: host+"registrationworkspace/",
                    method: .post,
                    parameters: parameters,
                    headers: headers)
        { (json) in
            if let err = json["errorMessage"].string {
                completion(false, NSError(domain: err, code: 0, userInfo: nil))
            } else {
                completion(true, nil)
            }
        }
    }
    
    public func eazyPayRegistration(mobilePhone: String, completion: @escaping WSResponseHandler) {
        let parameters: Parameters = [
            "login": mobilePhone,
            "mobilePhone": mobilePhone]
        let headers = ["Content-Type": "application/json"]
        fireRequset(url: host+"easypay/registration/",
                    method: .post,
                    parameters: parameters,
                    headers: headers)
        { (json) in
            if let errorMessage = json["errorMessage"].string {
                completion(JSON.null, NSError(domain: json["errorCode"].stringValue, code: 0, userInfo: ["error_name": errorMessage]))
            } else {
                completion(json, nil)
            }
        }
    }
    
    public func getMyProfile(completion: @escaping WSResponseHandler) {
        let accessToken = "?access_token="+KeychainSwift().get("access_token")!
        fireRequset(url: host+"user/workspace/profile"+accessToken,
                    method: .get,
                    parameters: nil,
                    headers: nil)
        { (json) in
            if !json["profile"].isEmpty {
                completion(json, nil)
            } else {
                completion(JSON.null, NSError(domain: "ERROR TO FETCH ALL FEED", code: 0, userInfo: nil))
            }
        }
    }
    
    public func getAllTickets(step: Int, completion: @escaping WSResponseHandler) {
        let accessToken = "?access_token="+KeychainSwift().get("access_token")!
        fireRequset(url: host+"user/workspace_tickets/\(step)"+accessToken,
                    method: .get,
                    parameters: nil,
                    headers: nil)
        { (json) in
            if !json["feed"].isEmpty {
                completion(json, nil)
            } else {
                completion(JSON.null, NSError(domain: "ERROR TO FETCH ALL FEED", code: 0, userInfo: nil))
            }
        }
    }
    
    public func getMyTickets(completion: @escaping WSResponseHandler) {
        let accessToken = "?access_token="+KeychainSwift().get("access_token")!
        fireRequset(url: host+"user/work_space_ticket/my_projects"+accessToken,
                    method: .get,
                    parameters: nil,
                    headers: nil)
        { (json) in
            if !json["myFeed"].isEmpty {
                completion(json, nil)
            } else {
                completion(JSON.null, NSError(domain: "ERROR TO FETCH MY FEED", code: 0, userInfo: nil))
            }
        }
    }
    
    public func getTicket(id: String, completion: @escaping WSResponseHandler) {
        let accessToken = "?access_token="+KeychainSwift().get("access_token")!
        fireRequset(url: host+"user/work_space_ticket/\(id)"+accessToken,
                    method: .get,
                    parameters: nil,
                    headers: nil)
        { (json) in
            if !json["ticket"].isEmpty {
                completion(json, nil)
            } else {
                completion(JSON.null, NSError(domain: "ERROR TO FETCH TICKET ON ID", code: 0, userInfo: nil))
            }
        }
    }
    
    public func createTicket(ticket: TicketToCreate, completion: @escaping WSBoolResponseHandler) {
        let parameters: Parameters = [
            "title": ticket.title,
            "text": ticket.text,
            "category": ticket.category,
            "underCategory": ticket.underCategory,
            "price": ticket.price
            ]
        let headers = ["Content-Type": "application/json"]
        let accessToken = "?access_token="+KeychainSwift().get("access_token")!
        fireRequset(url: host+"user/create_ticket"+accessToken,
                    method: .post,
                    parameters: parameters,
                    headers: headers)
        { (json) in
            if json["create"].boolValue {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "ERROR TO CREATE TICKET", code: 0, userInfo: nil))
            }
        }
    }
    
    public func createTicketComment(ticketId: String, text: String, money: Int, numberOfDays: Int, completion: @escaping WSBoolResponseHandler) {
        let parameters: Parameters = [
            "text": text,
            "money": money,
            "numberOfDays": numberOfDays
        ]
        let headers = ["Content-Type": "application/json"]
        let accessToken = "?access_token="+KeychainSwift().get("access_token")!
        fireRequset(url: host+"user/work_space_ticket/\(ticketId)/create_comment"+accessToken,
            method: .post,
            parameters: parameters,
            headers: headers)
        { (json) in
            print(json)
            if json["add"].boolValue {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "ERROR TO ADD COMMENT", code: 0, userInfo: nil))
            }
        }
    }
    
    
    
    fileprivate func fireRequset(url: String, method: HTTPMethod, parameters: Parameters?, headers: [String: String]?, completion: @escaping (JSON) -> Void) {
        Alamofire.request(url,
                          method: method,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON
            { response in
                print(response.request as Any)
                if let body = response.request?.httpBody {
                    print(String(data:body, encoding: .utf8) as Any)
                }
                print(String(data: response.data!, encoding: .utf8) as Any)
                let JSONresultValue = JSON(response.data as Any)
                if let error = JSONresultValue.error {
                    print("500", error)
                    completion(JSON(["errorMessage": "server error"]))
                    return
                }
                print(JSONresultValue)
                completion(JSONresultValue)
        }
    }
    
}
