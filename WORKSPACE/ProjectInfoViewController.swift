//
//  ProjectInfoViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/7/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class ProjectInfoViewController: UIViewController {

    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var bidsButton: UIButton!
    @IBOutlet weak var positionIndicator: CUIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var ticketPosts: [TicketPost]?
    
    var ticket: Ticket? {
        didSet {
            self.getTicketInfo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getTicketInfo()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView() {
        
    }
    
    func animatePositionIndicator(to: ProjectIndicatorPosition) {
        let originX: CGFloat!
        let width: CGFloat!
        switch to {
        case .left:
            originX = self.infoButton.frame.origin.x
            width = self.infoButton.frame.width
        case .right:
            originX = self.bidsButton.frame.origin.x
            width = self.bidsButton.frame.width
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: .curveEaseOut, animations: {
            self.positionIndicator.frame = CGRect(x: originX, y: self.positionIndicator.frame.origin.y, width: width, height: self.positionIndicator.frame.height)
        }, completion: nil)
    }
    
    func getTicketInfo() {
        if ticket != nil {
            WSApi.shared.getTicket(id: "\(ticket!.id)", completion: { (json, error) in
                if let error = error {
                    print(error)
                    return
                }
                self.ticketPosts = [TicketPost]()
                for post in json["ticketPost"] {
                    let ticketPost = TicketPost(data: post.1)
                    self.ticketPosts?.append(ticketPost)
                }
                self.collectionView.reloadData()
            })
        }
    }
    
    @IBAction func openInformation(_ sender: Any) {
        let indexPath = IndexPath(item: 0, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.animatePositionIndicator(to: .left)
    }
    
    @IBAction func openBids(_ sender: Any) {
        let indexPath = IndexPath(item: 1, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.animatePositionIndicator(to: .right)
    }
    
    @IBAction func handleBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ProjectInfoViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "projectInfoCell", for: indexPath) as! ProjectInfoCell
            if self.ticket != nil {
                cell.ticket = self.ticket
            }
            cell.delegateController = self
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bidsCell", for: indexPath) as! BidsCell
            cell.ticketPosts = self.ticketPosts
            cell.backgroundColor = .blue
            return cell
        }
    }
    
}



class ProjectInfoCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let infoCellID = "InfoCellID"
    
    var ticket: Ticket! {
        didSet {
            self.collectionView.reloadData()
        }
    }

    weak var delegateController: ProjectInfoViewController?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var leftCommentButton: UIButton!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: infoCellID, for: indexPath) as! TicketInfoCell
        if ticket != nil {
            cell.ticket = self.ticket
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 621)
    }
    
    @IBAction func leftComment(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCommentViewController") as! AddCommentViewController
        vc.ticket = self.ticket
        self.delegateController?.show(vc, sender: self)
    }
    
}


class TicketInfoCell: UICollectionViewCell {
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var category: UILabel!
    
    var ticket: Ticket! {
        didSet {
            self.title.text = ticket.title
            self.category.text = "\(ticket.category) • \(ticket.underCategory)"
        }
    }
}












class BidsCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var ticketPosts: [TicketPost]? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ticketPosts?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TicketBid", for: indexPath) as! TicketBid
        cell.ticketPost = self.ticketPosts?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 200)
    }
    
}


class TicketBid: UICollectionViewCell {
    
    @IBOutlet weak var commentText: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var numverOfDays: UILabel!
    
    var ticketPost: TicketPost! {
        didSet {
            if ticketPost != nil {
                self.commentText.text = ticketPost.text
                self.phoneNumber.text = ticketPost.mobilePhone
                self.userName.text = ticketPost.name
                self.price.text = "\(ticketPost.money) гривень"
                self.numverOfDays.text = "\(ticketPost.numberOfDays) днів"
            }
        }
    }
    
}





















