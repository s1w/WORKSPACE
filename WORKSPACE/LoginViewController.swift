//
//  LoginViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/4/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    fileprivate func setupView() {
        self.setEndEditOnTap()
    }

    @IBAction func loginTapped(_ sender: UIButton) {
        guard let phone = self.phoneTextField.text else { return }
        guard let password = self.passwordTextField.text else { return }
        if phone.isEmpty { return }
        if password.isEmpty { return }
        self.loginButton.isEnabled = false
        WSApi.shared.login(phone: phone, password: password) { (isLogin, error) in
            self.loginButton.isEnabled = true
            if let error = error {
                print(error)
                let alert = UIAlertController(title: "Ой, щось не так 🤔", message: error.domain, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            if isLogin {
                print("SUCCESS LOGIN TO ACCAUNT")
                UserDefaults.standard.logIn()
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyMenuNavigationController") as! MyMenuNavigationController
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func registrationTapped(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        self.present(vc, animated: true, completion: nil)
    }

}






















