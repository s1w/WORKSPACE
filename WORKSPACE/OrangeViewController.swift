//
//  OrangeViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/5/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class OrangeViewController: UIViewController {
    
    weak var menu: MyMenuViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func openMenu(_ sender: Any) {
        self.menu?.menuTapped()
    }
    
}
