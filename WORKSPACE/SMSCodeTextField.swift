//
//  SMSCodeTextField.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/14/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

@IBDesignable class SMSCodeTextField: UIView, UITextFieldDelegate {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var codeLength: Int = 4 {
        didSet {
            if codeLength < 2 { codeLength = 2 }
            self.textFieldLabel.text = createTextPlaceholders(count: codeLength)
        }
    }
    
    @IBOutlet weak var delegateController: RegistrationViewController?
    public var code: String = ""
    
    private var textField: UITextField!
    private var textFieldLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView( )
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    private func setupView() {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openKeyboard)))
        
        self.textField = UITextField()
        self.textField.tintColor = .clear
        self.textField.backgroundColor = .clear
        self.textField.textColor = .clear
        self.textField.textAlignment = .center
        self.textField.keyboardType = .numberPad
        self.textField.keyboardAppearance = .default
        self.textField.delegate = self
        self.textField.translatesAutoresizingMaskIntoConstraints = false
        
        self.textFieldLabel = UILabel()
        self.textFieldLabel.textAlignment = .center
        self.textFieldLabel.textColor = .white
        self.textFieldLabel.font = UIFont.systemFont(ofSize: 20)
        self.textFieldLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(textField)
        self.addSubview(textFieldLabel)
        
        self.addConstraints([
            NSLayoutConstraint(item: self.textField, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.textField, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.textField, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.textField, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0),
            
            NSLayoutConstraint(item: self.textFieldLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.textFieldLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.textFieldLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.textFieldLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0),
            ])
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.characters.count > 1 {
            return false
        }
        for char in string.unicodeScalars {
            if !CharacterSet.decimalDigits.contains(char) { return false }
        }
        if range.length == 0 {
            guard let range = textFieldLabel.text?.range(of: "-") else { return false }
            self.textFieldLabel.text = self.textFieldLabel.text?.replacingCharacters(in: range, with: string)
            self.code.append(string)
            if self.code.characters.count == self.codeLength {
                self.disable()
                self.textField.resignFirstResponder()
                self.delegateController?.registrationTapped(UIButton())
            }
            return true
        } else if range.length == 1 {
            guard var t = textFieldLabel.text else { return false }
            var array = Array(t.characters)
            for i in (0..<array.count).reversed() {
                if array[i] != " " && array[i] != "-" {
                    array[i] = "-"
                    self.code = self.code.substring(to: code.index(before: code.endIndex))
                    self.textFieldLabel.text = String(array)
                    return false
                }
            }
        }
        return true
    }
    
    func createTextPlaceholders(count: Int) -> String {
        if count == 4 {
            return "-  -  -  -"
        } else if count == 6 {
            return "-  -  -   -  -  -"
        } else if count == 8 {
            return "- - - -   - - - -"
        } else {
            var str = "-  "
            for _ in 0..<count-2 {
                str.append(str)
            }
            str.append("-")
            return str
        }
    }
    
    func openKeyboard() {
        self.textField.becomeFirstResponder()
    }
    
    func disable() {
        self.alpha = 0.5
    }
    
    func enable() {
        self.alpha = 1
    }
}
