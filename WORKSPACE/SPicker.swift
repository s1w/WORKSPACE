//
//  SPicker.swift
//  SPicker
//
//  Created by Yevhenii Veretennikov on 3/12/17.
//  Copyright © 2017 Yevhenii Veretennikov. All rights reserved.
//

import UIKit

class SPicker: UIView {
    
    public typealias SPickerCallBack = (String?) -> Void
    
    // MARK: - constants
    
    fileprivate let kPickerContentViewHeight     : CGFloat = 240
    fileprivate let kPickerSeparatorLineHeight   : CGFloat = 0.5
    fileprivate let kPickerButtonHeight          : CGFloat = 45
    fileprivate let kPickerButtonWidth           : CGFloat = 90
    fileprivate let kPickerHeight                : CGFloat = 200
    fileprivate let kPickerDoneButtonTag         : Int = 1
    fileprivate let kPickerContenteViewWidth     : CGFloat = UIScreen.main.bounds.width-16
    fileprivate let kPickerContentXOffset        : CGFloat = 8
    fileprivate let kPickerContentYOffset        : CGFloat = 12
    
    // MARK: - variables
    
    fileprivate var doneButton      : UIButton!
    fileprivate var cancelButton    : UIButton!
    fileprivate var picker          : UIPickerView!
    fileprivate var separatorLine   : UIView!
    fileprivate var highlighterLineTop: UIView!
    fileprivate var highlighterLineBot: UIView!
    fileprivate var backView        : UIView!
    fileprivate var rootView        : UIView!
    fileprivate var pickerDataSoruce: [String]!
    fileprivate var selectedValue   : String!
    fileprivate var completion      : SPickerCallBack?
    
    // MARK: - init block
    
    public init() {
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        self.alpha = 0
        self.setupView()
    }
    
    func setupView() {
        self.backView = UIView()
        self.backView.backgroundColor = UIColor(white: 0, alpha: 0.45)
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SPicker.close)))
        self.backView.frame = self.frame
        
        self.setupRootView()
        
        self.addSubview(backView)
        self.addSubview(rootView)
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.rootView.frame = CGRect(x: self.kPickerContentXOffset, y: UIScreen.main.bounds.height-self.kPickerContentViewHeight-self.kPickerContentYOffset, width: self.kPickerContenteViewWidth, height: self.kPickerContentViewHeight)
            self.alpha = 1
        }, completion:  nil)
    }
    
    func show(doneTitle: String, cancelTitle: String, rows: [String], completion: @escaping SPickerCallBack) {
        self.doneButton.setTitle(doneTitle, for: .normal)
        self.cancelButton.setTitle(cancelTitle, for: .normal)
        self.pickerDataSoruce = rows
        self.completion = completion
        self.selectedValue = pickerDataSoruce[0]
        
        guard let appDelegate = UIApplication.shared.delegate else { fatalError() }
        guard let window = appDelegate.window else { fatalError() }
        window?.addSubview(self)
        window?.bringSubview(toFront: self)
        window?.endEditing(true)
    }
    
    func close() {
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.rootView.frame = CGRect(x: self.kPickerContentXOffset, y: UIScreen.main.bounds.height, width: self.kPickerContenteViewWidth, height: self.kPickerContentViewHeight)
            self.alpha = 0
        }) { (finished) in
            for view in self.subviews {
                view.removeFromSuperview()
            }
            self.removeFromSuperview()
        }
    }
    
    func setupRootView() {
        self.rootView = UIView()
        self.rootView.backgroundColor = .white
        self.rootView.frame = CGRect(x: self.kPickerContentXOffset, y: UIScreen.main.bounds.height, width: kPickerContenteViewWidth, height: kPickerContentViewHeight)
        self.rootView.alpha = 0.99
        self.rootView.layer.cornerRadius = 18
        self.rootView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(viewWillDrag(_:))))
        self.rootView.isUserInteractionEnabled = true
        
        self.separatorLine = UIView()
        self.separatorLine.backgroundColor = UIColor.gray
        self.separatorLine.alpha = 0.7
        self.separatorLine.frame = CGRect(x: 0, y: kPickerButtonHeight, width: kPickerContenteViewWidth, height: kPickerSeparatorLineHeight)
        
        self.highlighterLineTop = UIView()
        self.highlighterLineTop.backgroundColor = UIColor.lightGray
        self.highlighterLineTop.alpha = 0.7
        self.highlighterLineTop.frame = CGRect(x: 0, y: kPickerContentViewHeight/2+42, width: kPickerContenteViewWidth, height: kPickerSeparatorLineHeight)
        
        self.highlighterLineBot = UIView()
        self.highlighterLineBot.backgroundColor = UIColor.lightGray
        self.highlighterLineBot.alpha = 0.7
        self.highlighterLineBot.frame = CGRect(x: 0, y: kPickerContentViewHeight/2+8, width: kPickerContenteViewWidth, height: kPickerSeparatorLineHeight)
        
        self.doneButton = UIButton(type: .system)
        self.doneButton.tag = kPickerDoneButtonTag
        self.doneButton.addTarget(self, action: #selector(SPicker.buttonTapped), for: .touchUpInside)
        self.doneButton.frame = CGRect(x: UIScreen.main.bounds.width-10-8-kPickerButtonWidth, y: 0, width: kPickerButtonWidth, height: kPickerButtonHeight)
        
        self.picker = UIPickerView(frame: CGRect(x: 0, y: kPickerButtonHeight, width: kPickerContenteViewWidth, height: kPickerHeight))
        self.picker.delegate = self
        self.picker.dataSource = self
        
        self.cancelButton = UIButton(type: .system)
        self.cancelButton.addTarget(self, action: #selector(SPicker.buttonTapped), for: .touchUpInside)
        self.cancelButton.frame = CGRect(x: 10, y: 0, width: kPickerButtonWidth, height: kPickerButtonHeight)
        
        self.rootView.addSubview(separatorLine)
        self.rootView.addSubview(highlighterLineTop)
        self.rootView.addSubview(highlighterLineBot)
        self.rootView.addSubview(picker)
        self.rootView.addSubview(doneButton)
        self.rootView.addSubview(cancelButton)
    }
    
    func buttonTapped(sender: UIButton!) {
        if sender.tag == kPickerDoneButtonTag {
            self.completion?(selectedValue)
        } else {
            self.completion?(nil)
        }
        self.close()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func viewWillDrag(_ recognizer: UIPanGestureRecognizer) {
        let t = recognizer.translation(in: rootView)
        switch recognizer.state {
        case .changed:
            rootView.frame = CGRect(x: self.kPickerContentXOffset, y: UIScreen.main.bounds.height-self.kPickerContentViewHeight-self.kPickerContentYOffset+t.y/2, width: self.kPickerContenteViewWidth, height: self.kPickerContentViewHeight)
            if t.y > 0 {
                self.backView.backgroundColor = UIColor(white: 0, alpha: 0.45 - t.y*0.45/100)
            }
        case .ended:
            UIView.animate(withDuration: 0.4, animations: {
                self.backView.backgroundColor = UIColor(white: 0, alpha: 0.45)
            })
            if t.y > kPickerContentViewHeight/2 {
                self.close()
            } else {
                self.openPicker()
            }
        default: return
        }
    }
    
    func openPicker() {
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.rootView.frame = CGRect(x: self.kPickerContentXOffset, y: UIScreen.main.bounds.height-self.kPickerContentViewHeight-self.kPickerContentYOffset, width: self.kPickerContenteViewWidth, height: self.kPickerContentViewHeight)
        }, completion: nil)
    }
    
}


extension SPicker: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSoruce.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSoruce[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedValue = pickerDataSoruce[row]
    }
}
