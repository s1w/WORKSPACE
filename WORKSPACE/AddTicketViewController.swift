//
//  AddTicketViewController.swift
//  WORKSPACE
//
//  Created by Yevhenii Veretennikov on 6/5/17.
//  Copyright © 2017 ENSIES. All rights reserved.
//

import UIKit

class AddTicketViewController: UIViewController {

    @IBOutlet weak var titleTextField: CUITextField!
    @IBOutlet weak var textTextField: UITextView!
    @IBOutlet weak var categoryTextField: CUITextField!
    @IBOutlet weak var underCategoryTextField: CUITextField!
    @IBOutlet weak var priceTextField: CUITextField!
    @IBOutlet weak var infoViewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEndEditOnTap()
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createTicket(_ sender: Any) {
        
        guard let title = titleTextField.text else { return }
        guard let text = textTextField.text else { return }
        guard let category = categoryTextField.text else { return }
        guard let underCategory = underCategoryTextField.text else { return }
        guard let price = priceTextField.text else { return }
        
        var errorMessage = ""
        
        if title.isEmpty {
            errorMessage = "Введіть назву проекту"
        } else if text.isEmpty {
            errorMessage = "Додайте опис до проекту"
        } else if category.isEmpty {
            errorMessage = "Вкажіть категорію проекту"
        } else if underCategory.isEmpty {
            errorMessage = "Вкажіть підкатегорію проекту"
        } else if price.isEmpty {
            errorMessage = "Вкажіть вартість проекту"
        }
        
        if !errorMessage.isEmpty {
            let alert = UIAlertController(title: "Ой, щось не так 🤔", message: errorMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        
        let ticket = TicketToCreate()
        ticket.title = title
        ticket.text = text
        ticket.category = category
        ticket.underCategory = underCategory
        ticket.price = Float(price)!
        
        WSApi.shared.createTicket(ticket: ticket) { (isCreated, error) in
            if let error = error {
                print(error)
                return
            }
            if isCreated {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

}




extension AddTicketViewController: UITextFieldDelegate {
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == self.titleTextField {
//            self.offsetContsiner(by: -10)
//        } else if textField == self.categoryTextField {
//            self.offsetContsiner(by: -100)
//        } else if textField == self.underCategoryTextField {
//            self.offsetContsiner(by: -150)
//        } else {
//            self.offsetContsiner(by: -180)
//        }
//        return true
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.titleTextField {
            self.textTextField.becomeFirstResponder()
        } else if textField == self.categoryTextField {
            self.underCategoryTextField.becomeFirstResponder()
        } else if textField == self.underCategoryTextField {
            self.priceTextField.becomeFirstResponder()
        } else {
            self.endEdit()
        }
        return true
    }
    
//    private func offsetContsiner(by: CGFloat) {
//        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
//            self.infoViewContainer.transform = CGAffineTransform(translationX: 0, y: by)
//        }, completion: nil)
//    }
    
}





















































